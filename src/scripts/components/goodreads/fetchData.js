'use strict';

const Reflux = require('reflux');
const jsonp = require('browser-jsonp');

const fetchData = Reflux.createAction({ asyncResult: true });

fetchData.listenAndPromise(settings => {
  return new Promise((resolve, reject) => {
    jsonp({
      url: `https://pipes.yahoo.com/pipes/pipe.run?_id=4a47fdbfda58f3ee15986c902dbe5c30&_render=json&url=http%3A%2F%2Fwww.goodreads.com%2Freview%2Flist%2F${settings.id}.xml%3Fkey%3DbzTGFjoYxZfmYoquLEHMw%26v%3D2%26shelf%3Dread%26per_page%3D3%26sort%3Ddate_read%26order%3Dd`,
      callbackName: '_callback',
      success: resolve,
      error: reject
    });
  });
});

module.exports = fetchData; 