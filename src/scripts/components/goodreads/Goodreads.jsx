'use strict';

var React = require('react/addons');
var List = require('../List');

var Goodreads = React.createClass({
  render: function () {
    const books = this.props.data.map(review => (
      <figure key={review.url}>
        <a href={review.url} title="View this book on Goodreads">
          <img src={review.book.image_url} alt="" />
        </a>
        <figcaption>
          <a href={review.url} title="View this book on Goodreads">
            {review.book.title}
          </a>
        </figcaption>
      </figure>
    ));
    return (
        <List ref="list"
              source={`https://www.goodreads.com/user/show/${this.props.settings.id}`}
              title="Goodreads profile"
              heading="Recently read"
              className="pictureReel">
          {books}
        </List>
      );
  }
});

module.exports = Goodreads; 

