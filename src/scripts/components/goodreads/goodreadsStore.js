'use strict';

const Reflux = require('reflux');
const fetchData = require('./fetchData');
const jsen = require('jsen');
const isValid = jsen(require('./schema.json'));

const goodreadsStore = Reflux.createStore({
  init(){
    this.books = [];
    this.listenTo(fetchData.completed, this.output);
  },
  output(data){
    const books = data.value.items[0].reviews.review;
    if(!isValid(books)){
      return false;
    }
    this.books = this.books.concat(books);
    this.trigger('goodreads', this.books);
  }
});

module.exports = goodreadsStore;
