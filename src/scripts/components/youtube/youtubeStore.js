'use strict';

const Reflux = require('reflux');
const fetchData = require('./fetchData');
const jsen = require('jsen');
const isValid = jsen(require('./schema.json'));

const youtubeStore = Reflux.createStore({
  init(){
    this.videos = [];
    this.listenTo(fetchData.completed, this.output);
  },
  output(data){
    if(!isValid(data.value.items[0].entry)){
      return false;
    }
    this.videos = this.videos.concat(data.value.items[0].entry);
    this.trigger('youtube', this.videos);
  }
});

module.exports = youtubeStore;
