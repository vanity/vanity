'use strict';

var React = require('react/addons');
var List = require('../List');

var Youtube = React.createClass({
  render() {
    const videos = this.props.data.map(video => (
    <figure key={video.link[0].href}>
      <a href={video.link[0].href} title={`View ${video.title} on Youtube`}>
        <img src={video['media:group']['media:thumbnail'][0].url} />
      </a>
      <figcaption>
        <a href={video.link[0].href} title={`View ${video.title} on Youtube`}>
          {video.title}
        </a>
      </figcaption>
    </figure>));
    return (
        <List ref="list"
              source={`https://www.youtube.com/user/${this.props.settings.username}`}
              title="Youtube profile"
              heading="Favourite videos"
              className="pictureReel">
          {videos}
        </List>
      );
  }
});

module.exports = Youtube; 

