'use strict';

const Reflux = require('reflux');
const jsonp = require('browser-jsonp');

const fetchData = Reflux.createAction({ asyncResult: true });

fetchData.listenAndPromise(settings => {
  return new Promise((resolve, reject) => {
    jsonp({
      url: `https://pipes.yahoo.com/pipes/pipe.run?_id=4a47fdbfda58f3ee15986c902dbe5c30&_render=json&url=http%3A%2F%2Fgdata.youtube.com%2Ffeeds%2Fapi%2Fusers%2F${settings.username}%2Ffavorites%3Fv%3D2%26max-results%3D3`,
      callbackName: '_callback',
      success: resolve,
      error: reject
    });
  });
});

module.exports = fetchData; 