'use strict';

const Reflux = require('reflux');
const jsonp = require('browser-jsonp');

const fetchData = Reflux.createAction({ asyncResult: true });

fetchData.listenAndPromise(settings => {
  return new Promise((resolve, reject) => {
    jsonp({
      url: `https://www.reddit.com/user/${settings.username}/liked/.json`,
      callbackName: 'jsonp',
      success: resolve,
      error: reject
    });
  });
});

module.exports = fetchData; 