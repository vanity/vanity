'use strict';

const Reflux = require('reflux');
const fetchData = require('./fetchData');
const jsen = require('jsen');
const isValid = jsen(require('./schema.json'));

const redditStore = Reflux.createStore({
  init(){
    this.likes = [];
    this.listenTo(fetchData.completed, this.output);
  },
  output(data){
    const likes = data.data.children.slice(0, 5);
    if(!isValid(likes)){
      return false;
    }
    this.likes = this.likes.concat(likes);
    this.trigger('reddit', this.likes);
  }
});

module.exports = redditStore;
