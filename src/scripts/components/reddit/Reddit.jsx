'use strict';

const React = require('react/addons');
const List = require('../List');

const Reddit = React.createClass({
  render(){
    const likes = this.props.data.map(like => (<a key={like.data.url} href={like.data.url}>{like.data.title}</a>));
    return (
        <List ref="list" source={`https://www.reddit.com/user/${this.props.settings.username}`} title="reddit profile" heading="Upvoted">
          {likes}
        </List>
      );
  }
});

module.exports = Reddit; 
