'use strict';

const Reflux = require('reflux');
import 'whatwg-fetch';

const fetchData = Reflux.createAction({ asyncResult: true });

fetchData.listenAndPromise(settings => {
  return fetch(`https://api.jamendo.com/get2/id+name+url+image+artist_name/album/json/album_user_starred/?user_idstr=${settings.username}&n=3&order=starreddate_desc`)
  .then(response => response.json());
});

module.exports = fetchData;