'use strict';

const React = require('react/addons');
const List = require('../List');

const Jamendo = React.createClass({
  render() {
    const albums = this.props.data.map(album => {
      return (
        <figure key={album.url}>
          <a href={album.url} title={`View ${album.artist_name} - ${album.name} on Jamendo`}>
            <img src={album.image} />
          </a>
          <figcaption>
            <a href={album.url} title={`View ${album.artist_name} - ${album.name} on Jamendo`}>
              {album.name}
            </a>
          </figcaption>
        </figure>);
    });
    return (
        <List ref="list" source={`https://www.jamendo.com/user/${this.props.settings.username}`}
              title="Jamendo profile"
              heading="Recently favourited albums"
              className="pictureReel">
          {albums}
        </List>
      );
  }
});

module.exports = Jamendo; 

