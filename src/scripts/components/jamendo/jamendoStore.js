'use strict';

const Reflux = require('reflux');
const fetchData = require('./fetchData');
const jsen = require('jsen');
const isValid = jsen(require('./schema.json'));

const jamendoStore = Reflux.createStore({
  init(){
    this.albums = [];
    this.listenTo(fetchData.completed, this.output);
  },
  output(data){
    if(!isValid(data)){
      return false;
    }
    this.albums = this.albums.concat(data);
    this.trigger('jamendo', this.albums);
  }
});

module.exports = jamendoStore;
