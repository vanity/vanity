'use strict';

const Reflux = require('reflux');
const fetchData = require('./fetchData');
const jsen = require('jsen');
const isValid = jsen(require('./schema.json'));

const _23Store = Reflux.createStore({
  init(){
    this.photos = [];
    this.listenTo(fetchData.completed, this.output);
  },
  output(data){
    const photos = data.value.items.slice(0, 3);
    if(!isValid(photos)){
      return false;
    }
    this.photos = this.photos.concat(photos);
    this.trigger('23', this.photos);
  }
});

module.exports = _23Store;
