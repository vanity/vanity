'use strict';

var React = require('react/addons');
var List = require('../List');

var _23 = React.createClass({
  render: function () {
    const photos = this.props.data.map(photo => {
      return (
        <a href={photo.link} title="View this photo on 23" key={photo.link}>
          <img src={photo['media:thumbnail'].url} />
        </a>
      );
    });
    return (
        <List ref="list"
              source={`http://www.23hq.com/${this.props.settings.username}`}
              title="23 profile"
              heading="Latest photos"
              className="pictureReel">
          {photos}
        </List>
      );
  }
});

module.exports = _23; 

