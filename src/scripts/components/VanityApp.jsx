'use strict';

const React = require('react/addons');
const ReactCSSTransitionGroup = React.addons.CSSTransitionGroup;
const Reflux = require('reflux');
const Gravatar = require('./Gravatar');
import 'whatwg-fetch';

let listStore = require('./listStore');

// CSS
require('../../styles/normalize.css');
require('../../styles/main.css');

const VanityApp = React.createClass({
  mixins: [Reflux.connect(listStore, 'lists')],
  getInitialState: () => ({ lists: new Map(), settings: {}, profile: { name: 'My profile', gravatar: {} } }),
  componentDidMount(){
    fetch(this.props.settingsFile)
    .then(response => response.json())
    .then(data => {
      const lists = Object.keys(data.services).map(key => {
        if('gravatar' === key){
          // Gravatar doesn't display a list, but loads the avatar as favicon
          return;
        }
        if(['picasa', 'gitorious', 'freelish', 'diaspora', 'twitter', 'identica'].indexOf(key) !== -1){
          // These services have been phased out or are no longer supported by Vanity
          return;
        }
        let store = require(`./${key}/${key}Store`);
        listStore.listenTo(store, listStore.addList);
        let fetchData = require(`./${key}/fetchData`);
        fetchData(data.services[key]);
      })
      .filter(card => typeof(card) !== 'undefined');
      
      const profile = {
        name: data.name,
        gravatar: data.services.gravatar
      };
      document.title = data.name;

      this.setState({ lists, profile, settings: data });
    });
  },
  render(){
    const lists = Array.from(this.state.lists.keys()).map(listType => {
      if(this.state.lists.get(listType).length === 0){
        // This list is empty
        return false;
      }
      const attributes = { data: this.state.lists.get(listType), key: listType, settings: this.state.settings.services[listType] };
      const elementName = listType.substring(0, 1).toUpperCase() + listType.substring(1);
      return React.createElement(require(`./${listType}/${elementName}`), attributes);
    })
    // Only include non-empty lists
    .filter(list => list !== false);
    return (
      <div id="container">
        <header id="siteHeader">
          <h1>{this.state.profile.name}</h1>
          <p>This page collects what I'm up to from various sources all over the internet.</p>
          <Gravatar settings={this.state.profile.gravatar} />
        </header>
        <div id="lists">
          <ReactCSSTransitionGroup transitionName="list">
            {lists}
          </ReactCSSTransitionGroup>
        </div>
        <footer id="poweredBy">
          Powered by <a href="http://Vanity.TuxFamily.org">Vanity</a>
		</footer>
      </div>
    );
  }
});

module.exports = VanityApp;
