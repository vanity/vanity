'use strict';

const Reflux = require('reflux');
const jsonp = require('browser-jsonp');

const fetchData = Reflux.createAction({ asyncResult: true });

fetchData.listenAndPromise(settings => {
  return new Promise((resolve, reject) => {
    jsonp({
      url: `https://feeds.delicious.com/v2/json/${settings.username}?count=5`,
      success: resolve,
      error: reject
    });
  });
});

module.exports = fetchData; 