'use strict';

const React = require('react/addons');
const List = require('../List');

const Delicious = React.createClass({
  render(){
    const bookmarks = this.props.data.map(bookmark => (<a key={bookmark.u} href={bookmark.u}>{bookmark.d}</a>));
    return (
        <List ref="list" source={`https://www.delicious.com/${this.props.settings.username}`} title="Delicious profile" heading="Recently bookmarked">
          {bookmarks}
        </List>
      );
  }
});

module.exports = Delicious; 

