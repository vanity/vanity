'use strict';

const Reflux = require('reflux');
const fetchData = require('./fetchData');
const jsen = require('jsen');
const isValid = jsen(require('./schema.json'));

const deliciousStore = Reflux.createStore({
  init(){
    this.bookmarks = [];
    this.listenTo(fetchData.completed, this.output);
  },
  output(data){
    if(!isValid(data)){
      return false;
    }
    this.bookmarks = this.bookmarks.concat(data);
    this.trigger('delicious', this.bookmarks);
  }
});

module.exports = deliciousStore;
