'use strict';

const Reflux = require('reflux');

const listStore = Reflux.createStore({
  init(){
    this.lists = new Map();
  },
  addList(listType, data){
    this.lists.set(listType, data);
    this.trigger(this.lists);
  }
});

module.exports = listStore; 