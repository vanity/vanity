'use strict';

const React = require('react/addons');

require('../../styles/List.styl');

const List = React.createClass({
  render(){
    const listItems = React.Children.map(this.props.children, child => (<li>{child}</li>));
    if(listItems.length === 0){
      // Only render the lists when the data has been loaded.
      // If something went wrong with fetching the data, it's no disaster to not show this list.
      return false;
    }
    return (
        <article key={this.props.source} className={this.props.className}>
          <h2>{this.props.heading}</h2>
          <ol>
            {listItems}
          </ol>
          <footer>
            <a href={this.props.source} title={`View my ${this.props.title}`} className="viewProfile">My {this.props.title}</a>
          </footer>
        </article>
      );
  }
});

module.exports = List; 

