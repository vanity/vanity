'use strict';

const React = require('react/addons');
const List = require('../List');

const Librefm = React.createClass({
  render(){
    const songs = this.props.data.map(track => (<a key={track.mbid + track.artist.name + track.name} href={track.url}>{track.artist.name} - {track.name}</a>));
    return (
        <List ref="list" source={`https://www.libre.fm/user/${this.props.settings.username}`} title="Libre.fm profile" heading="Favourite songs">
          {songs}
        </List>
      );
  }
});

module.exports = Librefm; 

