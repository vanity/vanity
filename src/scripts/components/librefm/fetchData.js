'use strict';

const Reflux = require('reflux');
const jsonp = require('browser-jsonp');

const fetchData = Reflux.createAction({ asyncResult: true });

fetchData.listenAndPromise(settings => {
  return new Promise((resolve, reject) => {
    jsonp({
      url: `https://alpha.libre.fm/2.0/?method=user.gettoptracks&user=${settings.username}&period=3month&format=json`,
      success: resolve,
      error: reject
    });
  });
});

module.exports = fetchData; 