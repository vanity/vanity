'use strict';

const Reflux = require('reflux');
const fetchData = require('./fetchData');
const jsen = require('jsen');
const isValid = jsen(require('./schema.json'));

const librefmStore = Reflux.createStore({
  init(){
    this.songs = [];
    this.listenTo(fetchData.completed, this.output);
  },
  output(data){
  if(!isValid(data.toptracks.track)){
    return false;
  }
    this.songs = this.songs.concat(data.toptracks.track);
    this.trigger('librefm', this.songs);
  }
});

module.exports = librefmStore;
