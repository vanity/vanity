'use strict';

const React = require('react/addons');
const List = require('../List');

const Flickr = React.createClass({
  render(){
    const photos = this.props.data.map(photo => (
      <a href={`https://www.flickr.com/photos/${this.props.settings.id}/${photo.id}/in/photostream`} key={photo.id} title="View this photo on Flickr">
        <img src={photo.url_t} />
      </a>
    ));
    return (
        <List ref="list" source={`https://www.flickr.com/photos/${this.props.settings.id}`}
              title="Flickr profile"
              heading="Latest photos"
              className="pictureReel">
          {photos}
        </List>
      );
  }
});

module.exports = Flickr; 

