'use strict';

const Reflux = require('reflux');
const fetchData = require('./fetchData');
const jsen = require('jsen');
const isValid = jsen(require('./schema.json'));

const flickrStore = Reflux.createStore({
  init(){
    this.photos = [];
    this.listenTo(fetchData.completed, this.output);
  },
  output(data){
    if(!isValid(data.photos.photo)){
      return false;
    }
    this.photos = this.photos.concat(data.photos.photo);
    this.trigger('flickr', this.photos);
  }
});

module.exports = flickrStore;
