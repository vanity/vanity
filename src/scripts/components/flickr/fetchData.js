'use strict';

const Reflux = require('reflux');
const jsonp = require('browser-jsonp');

const fetchData = Reflux.createAction({ asyncResult: true });

fetchData.listenAndPromise(settings => {
  return new Promise((resolve, reject) => {
    jsonp({
      url: `https://api.flickr.com/services/rest/?format=json&method=flickr.people.getPublicPhotos&api_key=44f7f1d9a3acef2569b8addc0f933243&per_page=3&extras=url_t&user_id=${settings.id}`,
      callbackName: 'jsoncallback',
      success: resolve,
      error: reject
    });
  });
});

module.exports = fetchData; 