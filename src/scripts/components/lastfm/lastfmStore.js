'use strict';

const Reflux = require('reflux');
const fetchData = require('./fetchData');
const jsen = require('jsen');
const isValid = jsen(require('./schema.json'));

const lastfmStore = Reflux.createStore({
  init(){
    this.songs = [];
    this.listenTo(fetchData.completed, this.output);
  },
  output(data){
    if(!isValid(data.toptracks.track)){
      return false;
    }
    this.songs = this.songs.concat(data.toptracks.track);
    this.trigger('lastfm', this.songs);
  }
});

module.exports = lastfmStore;
