'use strict';

const React = require('react/addons');
const List = require('../List');

const Lastfm = React.createClass({
  render(){
    const songs = this.props.data.map(track => (<a key={track.mbid + track.artist.name + track.name} href={track.url}>{track.artist.name} - {track.name}</a>));
    return (
        <List ref="list" source={`https://www.last.fm/user/${this.props.settings.username}`} title="Last.fm profile" heading="Favourite songs">
          {songs}
        </List>
      );
  }
});

module.exports = Lastfm; 

