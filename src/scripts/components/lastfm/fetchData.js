'use strict';

const Reflux = require('reflux');
import 'whatwg-fetch';

const fetchData = Reflux.createAction({ asyncResult: true });

fetchData.listenAndPromise(settings => {
  return fetch(`https://ws.audioscrobbler.com/2.0/?method=user.gettoptracks&user=${settings.username}&api_key=3578c376cbe926fdf8aecc97fa8eed27&limit=5&period=3month&format=json`)
  .then(response => response.json());
});

module.exports = fetchData; 