'use strict';

const Reflux = require('reflux');
const jsonp = require('browser-jsonp');

const fetchData = Reflux.createAction({ asyncResult: true });

fetchData.listenAndPromise(settings => {
  return new Promise((resolve, reject) => {
    jsonp({
      url: `https://pipes.yahoo.com/pipes/pipe.run?_id=94ace3e6f2dc55156d6a3012d772dc99&_render=json&feed=http%3A%2F%2Ffeeds.launchpad.net%2F%7E${settings.username}%2Frevisions.atom`,
      callbackName: '_callback',
      success: resolve,
      error: reject
    });
  });
});

module.exports = fetchData; 