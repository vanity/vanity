'use strict';

const React = require('react/addons');
const List = require('../List');

const Launchpad = React.createClass({
  render(){
    const activities = this.props.data.map(activity => activity.title);
    return (
        <List ref="list" source={`https://launchpad.net/~${this.props.settings.username}`} title="Launchpad profile" heading="Latest development activity">
          {activities}
        </List>
      );
  }
});

module.exports = Launchpad; 
