'use strict';

const React = require('react/addons');
const List = require('../List');

const stripUsername = (title, username) => title.replace(username, '');
const capitaliseFirstLetter = title => title.substring(0, 1).toUpperCase() + title.substring(1);

const Github = React.createClass({
  render(){
    const activities = this.props.data.map(activity => (<a key={activity.link} href={activity.link}>{capitaliseFirstLetter(stripUsername(activity.title, this.props.settings.username).trim())}</a>));
    return (
        <List ref="list" source={`https://github.com/${this.props.settings.username}`} title="GitHub profile" heading="Latest development activity">
          {activities}
        </List>
      );
  }
});

module.exports = Github; 
