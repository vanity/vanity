'use strict';

const Reflux = require('reflux');
const fetchData = require('./fetchData');
const jsen = require('jsen');
const isValid = jsen(require('./schema.json'));

const githubStore = Reflux.createStore({
  init(){
    this.activity = [];
    this.listenTo(fetchData.completed, this.output);
  },
  output(data){
    const activity = data.value.items.slice(0, 5);
    if(!isValid(activity)){
      return false;
    }
    this.activity = this.activity.concat(activity);
    this.trigger('github', this.activity);
  }
});

module.exports = githubStore;
