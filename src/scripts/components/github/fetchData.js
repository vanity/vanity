'use strict';

const Reflux = require('reflux');
const jsonp = require('browser-jsonp');

const fetchData = Reflux.createAction({ asyncResult: true });

fetchData.listenAndPromise(settings => {
  return new Promise((resolve, reject) => {
    jsonp({
      url: `https://pipes.yahoo.com/pipes/pipe.run?_id=94ace3e6f2dc55156d6a3012d772dc99&_render=json&feed=https%3A%2F%2Fgithub.com%2F${settings.username}.atom`,
      callbackName: '_callback',
      success: resolve,
      error: reject
    });
  });
});

module.exports = fetchData; 