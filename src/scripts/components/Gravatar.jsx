'use strict';

var React = require('react/addons');

require('../../styles/Gravatar.styl');

var Gravatar = React.createClass({
  render() {
    if(!this.props.settings.id || this.props.settings.id.length === 0){
      return false;
    }

    let favicon = document.createElement('link');
    favicon.rel = 'shortcut icon';
    favicon.href = `https://gravatar.com/avatar/${this.props.settings.id}.jpg?default=404&size=48`;
    document.getElementsByTagName('head')[0].appendChild(favicon);

    return (
      <img src={`https://gravatar.com/avatar/${this.props.settings.id}.jpg?default=404&size=100`} alt="" className="avatar" />
    );
  }
});

module.exports = Gravatar; 

