'use strict';

const Reflux = require('reflux');
const jsonp = require('browser-jsonp');

const fetchData = Reflux.createAction({ asyncResult: true });

fetchData.listenAndPromise(settings => {
  return new Promise((resolve, reject) => {
    jsonp({
      url: `https://vimeo.com/api/v2/${settings.username}/videos.json`,
      success: resolve,
      error: reject
    });
  });
});

module.exports = fetchData; 