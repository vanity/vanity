'use strict';

const React = require('react/addons');
const List = require('../List');

const Vimeo = React.createClass({
  render(){
    const videos = this.props.data.map(video => (
      <figure key={video.url}>
        <a href={video.url} title={`View ${video.title} on Vimeo`}>
          <img src={video.thumbnail_small} />
        </a>
        <figcaption>
          <a href={video.url} title={`View ${video.title} on Vimeo`}>
            {video.title}
          </a>
        </figcaption>
      </figure>
    ));
    return (
        <List ref="list"
              source={`https://www.vimeo.com/${this.props.settings.username}`}
              title="Vimeo profile"
              heading="Latest videos"
              className="pictureReel">
          {videos}
        </List>
      );
  }
});

module.exports = Vimeo; 

