'use strict';

const Reflux = require('reflux');
const fetchData = require('./fetchData');
const jsen = require('jsen');
const isValid = jsen(require('./schema.json'));

const vimeoStore = Reflux.createStore({
  init(){
    this.videos = [];
    this.listenTo(fetchData.completed, this.output);
  },
  output(data){
    if(!isValid(data)){
      return false;
    }
    this.videos = this.videos.concat(data);
    this.trigger('vimeo', this.videos);
  }
});

module.exports = vimeoStore;
