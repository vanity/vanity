'use strict';

const React = require('react/addons');
const VanityApp = require('./components/VanityApp');

React.render(<VanityApp settingsFile="settings.json" />, document.getElementById('content')); // jshint ignore:line