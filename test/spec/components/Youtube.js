'use strict';

describe('Youtube', function () {
  var React = require('react/addons');
  var Youtube, component;

  beforeEach(function () {
    Youtube = require('../../../src/scripts/components/youtube/Youtube');
    component = React.createElement(Youtube);
  });

  it('should create a new instance of Youtube', function () {
    expect(component).toBeDefined();
  });
});
