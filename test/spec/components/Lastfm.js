'use strict';

describe('Lastfm', function () {
  var React = require('react/addons');
  var Lastfm, component;

  beforeEach(function () {
    Lastfm = require('../../../src/scripts/components/lastfm/Lastfm');
    component = React.createElement(Lastfm);
  });

  it('should create a new instance of Lastfm', function () {
    expect(component).toBeDefined();
  });
});
