'use strict';

describe('Vimeo', function () {
  var React = require('react/addons');
  var Vimeo, component;

  beforeEach(function () {
    Vimeo = require('../../../src/scripts/components/vimeo/Vimeo');
    component = React.createElement(Vimeo);
  });

  it('should create a new instance of Vimeo', function () {
    expect(component).toBeDefined();
  });
});
