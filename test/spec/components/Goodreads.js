'use strict';

describe('Goodreads', function () {
  var React = require('react/addons');
  var Goodreads, component;

  beforeEach(function () {
    Goodreads = require('../../../src/scripts/components/goodreads/Goodreads');
    component = React.createElement(Goodreads);
  });

  it('should create a new instance of Goodreads', function () {
    expect(component).toBeDefined();
  });
});
