'use strict';

describe('Gravatar', function () {
  var React = require('react/addons');
  var Gravatar, component;

  beforeEach(function () {
    Gravatar = require('../../../src/scripts/components/Gravatar');
    component = React.createElement(Gravatar);
  });

  it('should create a new instance of Gravatar', function () {
    expect(component).toBeDefined();
  });
});
