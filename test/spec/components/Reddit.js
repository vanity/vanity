'use strict';

describe('Reddit', function () {
  var React = require('react/addons');
  var Reddit, component;

  beforeEach(function () {
    Reddit = require('../../../src/scripts/components/reddit/Reddit');
    component = React.createElement(Reddit);
  });

  it('should create a new instance of Reddit', function () {
    expect(component).toBeDefined();
  });
});
