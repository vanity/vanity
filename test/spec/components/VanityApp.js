'use strict';

describe('Main', function () {
  var React = require('react/addons');
  var VanityApp, component;

  beforeEach(function () {
    var container = document.createElement('div');
    container.id = 'content';
    document.body.appendChild(container);

    VanityApp = require('../../../src/scripts/components/VanityApp.jsx');
    component = React.createElement(VanityApp);
  });

  it('should create a new instance of VanityApp', function () {
    expect(component).toBeDefined();
  });
});
