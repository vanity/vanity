'use strict';

describe('Flickr', function () {
  var React = require('react/addons');
  var Flickr, component;

  beforeEach(function () {
    Flickr = require('../../../src/scripts/components/flickr/Flickr');
    component = React.createElement(Flickr);
  });

  it('should create a new instance of Flickr', function () {
    expect(component).toBeDefined();
  });
});
