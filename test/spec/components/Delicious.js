'use strict';

describe('Delicious', function () {
  var React = require('react/addons');
  var Delicious, component;

  beforeEach(function () {
    Delicious = require('../../../src/scripts/components/delicious/Delicious');
    component = React.createElement(Delicious);
  });

  it('should create a new instance of Delicious', function () {
    expect(component).toBeDefined();
  });
});
