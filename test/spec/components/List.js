'use strict';

describe('List', function () {
  var React = require('react/addons');
  var List, component;

  beforeEach(function () {
    List = require('../../../src/scripts/components/List.jsx');
    component = React.createElement(List);
  });

  it('should create a new instance of List', function () {
    expect(component).toBeDefined();
  });
});
