'use strict';

describe('Librefm', function () {
  var React = require('react/addons');
  var Librefm, component;

  beforeEach(function () {
    Librefm = require('../../../src/scripts/components/librefm/Librefm');
    component = React.createElement(Librefm);
  });

  it('should create a new instance of Librefm', function () {
    expect(component).toBeDefined();
  });
});
