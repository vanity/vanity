'use strict';

describe('Jamendo', function () {
  var React = require('react/addons');
  var Jamendo, component;

  beforeEach(function () {
    Jamendo = require('../../../src/scripts/components/jamendo/Jamendo');
    component = React.createElement(Jamendo);
  });

  it('should create a new instance of Jamendo', function () {
    expect(component).toBeDefined();
  });
});
