Vanity enables you to collect all your online activity onto a single page. An example website running Vanity is [Vinnl.nl](http://vinnl.nl/).

# Building
You will need `npm` and `grunt`. In the folder containing the source code, first run `npm install` to install all required packages.

To run an instance locally, you can run `grunt serve`; this will open a new browser tab running the current version of the code.

To generate a package to upload to your web server, run `grunt dist`. This will generate a folder `dist` containing `vanity.tar.gz`. The contents of this archive can be uploaded to your web server, and configured as described below.

# Setting it up
When you have a package containing Vanity (see [Building](#Building)), you can configure the user accounts of your desired web services in the file `settings.json`. It probably speaks for itself. If not, you can use [the web interface](http://vanity.tuxfamily.org/setup) to generate a configuration file. (Note: the rest of that website is out of date.)